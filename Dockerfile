FROM postgres:14

RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends curl; \
    curl -sSL https://techsupport.enterprisedb.com/api/repository/dl/default/release/deb | bash; \
    apt-get install -y --no-install-recommends postgresql-14-pglogical; \
    rm -rf /var/lib/apt/lists/*
